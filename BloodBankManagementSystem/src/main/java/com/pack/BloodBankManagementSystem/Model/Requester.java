package com.pack.BloodBankManagementSystem.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "requester")
public class Requester {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long requesterId;
	private String patientName;
	private String reqBloodGroup;
	private String city;
	private String doctorsName;
	private String hospName;
	private String Status;
	@Override
	public String toString() {
		return "Requester [requesterId=" + requesterId + ", patientName=" + patientName + ", reqBloodGroup="
				+ reqBloodGroup + ", city=" + city + ", doctorsName=" + doctorsName + ", hospName=" + hospName
				+ ", hospAddress=" + hospAddress + ", Date=" + Date + ", contactName=" + contactName
				+ ", contactNumber=" + contactNumber + ", contactEmaild=" + contactEmaild + ", Message=" + Message
				+ "]";
	}
	public Requester(String patientName, String reqBloodGroup, String city, String doctorsName, String hospName,
			String hospAddress, String date, String contactName, String contactNumber, String contactEmaild,
			String message) {
		super();
		this.patientName = patientName;
		this.reqBloodGroup = reqBloodGroup;
		this.city = city;
		this.doctorsName = doctorsName;
		this.hospName = hospName;
		this.hospAddress = hospAddress;
		Date = date;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.contactEmaild = contactEmaild;
		Message = message;
	}
	public Requester() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Requester(long requesterId, String patientName, String reqBloodGroup, String city, String doctorsName,
			String hospName, String hospAddress, String date, String contactName, String contactNumber,
			String contactEmaild, String message) {
		super();
		this.requesterId = requesterId;
		this.patientName = patientName;
		this.reqBloodGroup = reqBloodGroup;
		this.city = city;
		this.doctorsName = doctorsName;
		this.hospName = hospName;
		this.hospAddress = hospAddress;
		Date = date;
		this.contactName = contactName;
		this.contactNumber = contactNumber;
		this.contactEmaild = contactEmaild;
		Message = message;
	}
	private String hospAddress;
    private String Date;
    private String contactName;
    private String contactNumber;
    private String contactEmaild;
    private String Message;
	public long getRequesterId() {
		return requesterId;
	}
	public void setRequesterId(long requesterId) {
		this.requesterId = requesterId;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getReqBloodGroup() {
		return reqBloodGroup;
	}
	public void setReqBloodGroup(String reqBloodGroup) {
		this.reqBloodGroup = reqBloodGroup;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDoctorsName() {
		return doctorsName;
	}
	public void setDoctorsName(String doctorsName) {
		this.doctorsName = doctorsName;
	}
	public String getHospName() {
		return hospName;
	}
	public void setHospName(String hospName) {
		this.hospName = hospName;
	}
	public String getHospAddress() {
		return hospAddress;
	}
	public void setHospAddress(String hospAddress) {
		this.hospAddress = hospAddress;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getContactEmaild() {
		return contactEmaild;
	}
	public void setContactEmaild(String contactEmaild) {
		this.contactEmaild = contactEmaild;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
}
