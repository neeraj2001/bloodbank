package com.pack.BloodBankManagementSystem.Model;

 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

 


@Entity
@Table(name="donors")
public class Donor {

 

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long donor_id;
    private String name;
    private String email;
    private String password;
    private String contact;
    private String address;
    private String location;
    private String btype;
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private int age;
    public Long getDonor_id() {
        return donor_id;
    }
    public void setDonor_id(Long donor_id) {
        this.donor_id = donor_id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getContact() {
        return contact;
    }
    public void setContact(String contact) {
        this.contact = contact;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getBtype() {
        return btype;
    }
    public void setBtype(String btype) {
        this.btype = btype;
    }
    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "Donor [donor_id=" + donor_id + ", name=" + name + ", email=" + email + ", password=" + password
                + ", contact=" + contact + ", address=" + address + ", location=" + location + ", btype=" + btype
                + ", gender=" + gender + ", age=" + age + "]";
    }
    public Donor(Long donor_id, String name, String email, String password, String contact, String address,
            String location, String btype, Gender gender, int age) {
        super();
        this.donor_id = donor_id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.contact = contact;
        this.address = address;
        this.location = location;
        this.btype = btype;
        this.gender = gender;
        this.age = age;
    }
    public Donor(String name, String email, String password, String contact, String address, String location,
            String btype, Gender gender, int age) {
        super();
        this.name = name;
        this.email = email;
        this.password = password;
        this.contact = contact;
        this.address = address;
        this.location = location;
        this.btype = btype;
        this.gender = gender;
        this.age = age;
    }
    public Donor() {
        super();
        // TODO Auto-generated constructor stub
    }
    
}