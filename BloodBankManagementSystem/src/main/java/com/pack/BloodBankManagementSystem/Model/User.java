package com.pack.BloodBankManagementSystem.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {
	
	

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long id;
		@Override
		public String toString() {
			return "User [id=" + id + ", firstName=" + firstName + ", userfirstName=" + userfirstName + ", lastName="
					+ lastName + ", userLastName=" + userLastName + ", bloodGroup=" + bloodGroup + ", userBloodGroup="
					+ userBloodGroup + ", city=" + city + ", userCity=" + userCity + "]";
		}
		public User(String firstName, String userfirstName, String lastName, String userLastName, String bloodGroup,
				String userBloodGroup, String city, String userCity) {
			super();
			this.firstName = firstName;
			this.userfirstName = userfirstName;
			this.lastName = lastName;
			this.userLastName = userLastName;
			this.bloodGroup = bloodGroup;
			this.userBloodGroup = userBloodGroup;
			this.city = city;
			this.userCity = userCity;
		}
		public User() {
			super();
			// TODO Auto-generated constructor stub
		}
		public User(long id, String firstName, String userfirstName, String lastName, String userLastName,
				String bloodGroup, String userBloodGroup, String city, String userCity) {
			super();
			this.id = id;
			this.firstName = firstName;
			this.userfirstName = userfirstName;
			this.lastName = lastName;
			this.userLastName = userLastName;
			this.bloodGroup = bloodGroup;
			this.userBloodGroup = userBloodGroup;
			this.city = city;
			this.userCity = userCity;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getUserfirstName() {
			return userfirstName;
		}
		public void setUserfirstName(String userfirstName) {
			this.userfirstName = userfirstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getUserLastName() {
			return userLastName;
		}
		public void setUserLastName(String userLastName) {
			this.userLastName = userLastName;
		}
		public String getBloodGroup() {
			return bloodGroup;
		}
		public void setBloodGroup(String bloodGroup) {
			this.bloodGroup = bloodGroup;
		}
		public String getUserBloodGroup() {
			return userBloodGroup;
		}
		public void setUserBloodGroup(String userBloodGroup) {
			this.userBloodGroup = userBloodGroup;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getUserCity() {
			return userCity;
		}
		public void setUserCity(String userCity) {
			this.userCity = userCity;
		}
		private String firstName;
		private String userfirstName;
		private String lastName;
		private String userLastName;
		private String bloodGroup;
		private String userBloodGroup;
		private String city;
		private String userCity;


}
