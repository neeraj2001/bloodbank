package com.pack.BloodBankManagementSystem.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.BloodBankManagementSystem.Dao.RequesterRepository;
import com.pack.BloodBankManagementSystem.Dao.UserRegistrationRepository;
import com.pack.BloodBankManagementSystem.Dao.UserRepository;
import com.pack.BloodBankManagementSystem.Model.Donor;
import com.pack.BloodBankManagementSystem.Model.Requester;
import com.pack.BloodBankManagementSystem.Model.User;
import com.pack.BloodBankManagementSystem.Model.UserRegistration;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserRepository repository;

	@Autowired
	UserRegistrationRepository repository1;

	@Autowired
	RequesterRepository repository2;

	@PostMapping(value = "/BloodBankManagementPortal/User")
	public ResponseEntity<User> postuser(@RequestBody User user) {
		try {
			User _user = repository.save(new User(user.getFirstName(), user.getUserfirstName(),user.getLastName(),user.getUserLastName(),user.getBloodGroup(),user.getUserBloodGroup(),user.getCity(),user.getUserCity()));
			return new ResponseEntity<>(_user, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping("/BloodBankManagementPortal/User")
	public ResponseEntity<List<User>> getAllUsers() {
		List<User> users = new ArrayList<User>();
		try {
			repository.findAll().forEach(users::add);

			if (users.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(users, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/BloodBankManagementPortal/User/{bloodGroup}")
	public ResponseEntity<User> getUserByBloodGroup(@PathVariable("bloodGroup") String bloodGroup) {
		Optional<User> userData = repository.findByBloodGroup(bloodGroup);

		if (userData.isPresent()) {
			return new ResponseEntity<>(userData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


	@PostMapping("/BloodBankManagementPortal/Requestor/UserRegistration")
	public ResponseEntity<UserRegistration> postuserRegistration(@RequestBody UserRegistration userRegistration) {
		try {
			UserRegistration userRegistration1 = repository.save(new UserRegistration(userRegistration.getEmailId(),userRegistration.getUserName(),userRegistration.getPassword()));
			return new ResponseEntity<>(userRegistration, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		} 
	}

	@PostMapping("/BloodBankManagementPortal/Donor/DonarLogin")
	public ResponseEntity<Donor> postCustomer(@RequestBody Donor donor) {
		try {
			Donor _donor = repository
					.save(new Donor(donor.getName(), donor.getEmail(), donor.getPassword(), donor.getContact(), donor.getAddress(), donor.getLocation(), donor.getBtype(), donor.getGender(), donor.getAge()));
			return new ResponseEntity<>(_donor, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
		}
	}


}

