package com.pack.BloodBankManagementSystem.Dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.pack.BloodBankManagementSystem.Model.Donor;
import com.pack.BloodBankManagementSystem.Model.User;
import com.pack.BloodBankManagementSystem.Model.UserRegistration;

public interface UserRepository extends CrudRepository<User, Long> {

	User save(User user);

	Optional<User> findByBloodGroup(String bloodGroup);

	UserRegistration save(UserRegistration userRegistration);

	Donor save(Donor donor);

	


}
