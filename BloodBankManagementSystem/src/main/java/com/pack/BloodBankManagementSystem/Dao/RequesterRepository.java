package com.pack.BloodBankManagementSystem.Dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.BloodBankManagementSystem.Model.Requester;

public interface RequesterRepository extends CrudRepository<Requester,Long>{

}
