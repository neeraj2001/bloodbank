package com.pack.BloodBankManagementSystem.Dao;

import org.springframework.data.repository.CrudRepository;

public interface AdminRepository extends CrudRepository<String,Long> {

}
