package com.pack.BloodBankManagementSystem.Dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.BloodBankManagementSystem.Model.UserRegistration;

public interface UserRegistrationRepository extends CrudRepository<UserRegistration, Integer>{

}
