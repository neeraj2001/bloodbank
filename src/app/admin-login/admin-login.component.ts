import { Component, OnInit } from '@angular/core';
import { AdminserviceService } from '../adminservice.service';
import { Admin } from '../Admin';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  admin:Admin = new Admin();
  submitted:boolean = false;

  constructor(private adminService:AdminserviceService,private route:Router) { }

  ngOnInit(): void {
  }

  login(){
    this.adminService.adminLogin(this.admin)
    .subscribe(
      admin => {
        if(admin != null){
          this.submitted = true;
          this.route.navigate(['adminLogin']);
        }
        else{
          this.submitted = false;
        }
      }
    );
  }
  onSubmit(){
    this.login();
  }
}
