import { Component, OnInit } from '@angular/core';
import { DonarServiceService } from '../donar-service.service';
import { Donor } from '../Donor';

@Component({
  selector: 'app-donar-login',
  templateUrl: './donar-login.component.html',
  styleUrls: ['./donar-login.component.css']
})
export class DonarLoginComponent implements OnInit {

     
 
  donor: Donor= new Donor();
  submitted= false;
  DonarServiceService: any;
  constructor(privatedonorService: DonarServiceService) { }
   
  ngOnInit(): void {
    }
   
  newDonor(): void{
  this.submitted = false;
  this.donor = new Donor();
    }
   
  save(){
  this.DonarServiceService.createDonor(this.donor)
       .subscribe(
  data=> {
  console.log(data);
  this.submitted =  true;
         },
  error=>console.log(error));
  this.donor = new Donor();
    }
   
  onSubmit(){
  this.save();
    }
   
  }
