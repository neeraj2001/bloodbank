import { TestBed } from '@angular/core/testing';

import { DonarServiceService } from './donar-service.service';

describe('DonarServiceService', () => {
  let service: DonarServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DonarServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
