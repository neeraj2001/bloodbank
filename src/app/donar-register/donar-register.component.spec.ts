import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonarRegisterComponent } from './donar-register.component';

describe('DonarRegisterComponent', () => {
  let component: DonarRegisterComponent;
  let fixture: ComponentFixture<DonarRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonarRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonarRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
