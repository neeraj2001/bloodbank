import { EmailValidator } from '@angular/forms';

export class UserRegister{
    email:string;
    userName:string;
    password:string;
    confirmPassword:string;
}