export class Donor{
    donor_id: number;
    name: string;
    email: string;
    password: string;
    contact: number;
    address: string;
    location: string;
    btype: string;
    age: number;
    gender: string;
    uName:string;
}