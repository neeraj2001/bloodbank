import { Component, OnInit } from '@angular/core';
import { Donor } from '../Donor';
import { DonarServiceService } from '../donar-service.service';

@Component({
  selector: 'app-donar-logins',
  templateUrl: './donar-logins.component.html',
  styleUrls: ['./donar-logins.component.css']
})
export class DonarLoginsComponent implements OnInit {

  donor: Donor= new Donor();
  submitted= false;
  DonarServiceService: any;
  constructor(privatedonorService: DonarServiceService) { }
   
  ngOnInit(): void {
    }
   
  newDonor(): void{
  this.submitted = false;
  this.donor = new Donor();
    }
   
  save(){
  this.DonarServiceService.createDonor(this.donor)
       .subscribe(
  data=> {
  console.log(data);
  this.submitted =  true;
         },
  error=>console.log(error));
  this.donor = new Donor();
    }
   
  onSubmit(){
  this.save();
    }
   
  }
