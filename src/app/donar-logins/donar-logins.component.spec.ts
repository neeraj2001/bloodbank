import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DonarLoginsComponent } from './donar-logins.component';

describe('DonarLoginsComponent', () => {
  let component: DonarLoginsComponent;
  let fixture: ComponentFixture<DonarLoginsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DonarLoginsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DonarLoginsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
