import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { DonorComponent } from './donor/donor.component';
import { RequestorComponent } from './requestor/requestor.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { DonarRegisterComponent } from './donar-register/donar-register.component';
import { DonarLoginComponent } from './donar-login/donar-login.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { DonarLoginsComponent } from './donar-logins/donar-logins.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    AdminComponent,
    DonorComponent,
    RequestorComponent,
    UserLoginComponent,
    UserRegisterComponent,
    DonarRegisterComponent,
    DonarLoginComponent,
    AdminLoginComponent,
    DonarLoginsComponent     
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
