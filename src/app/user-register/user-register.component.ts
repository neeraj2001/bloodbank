import { Component, OnInit } from '@angular/core';
import { UserRegister } from '../userRegister';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-register',
  templateUrl: './user-register.component.html',
  styleUrls: ['./user-register.component.css']
})
export class UserRegisterComponent implements OnInit {

  userRegister: UserRegister=new UserRegister();
  submitted=false;
  UserService: any;

 
  constructor(private customerService : UserService) { }
 
  ngOnInit(): void {
  }
 
  newUserRegister():void{ 
    this.submitted=false;
    this.userRegister=new UserRegister(); 
  }
 
  save(){
    this.UserService.createuserRegister(this.userRegister) 
    .subscribe(
      data =>{
        console.log(data);
        this.submitted=true;
      },
      error => console.log(error));
      this.userRegister=new UserRegister();
  }
 
  onSubmit(){
    this.save();
  }
 
}
