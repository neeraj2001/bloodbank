import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:8081/api/BloodBankManagementPortal/Requestor/UserRegistration';

  constructor(private http: HttpClient) { }

  
  createuserRegister(userRegister: any): Observable<any>{
    return this.http.post(this.baseUrl, userRegister);
  }
}
   