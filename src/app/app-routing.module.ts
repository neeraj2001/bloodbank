import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { DonorComponent } from './donor/donor.component';
import { RequestorComponent } from './requestor/requestor.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { DonarLoginComponent } from './donar-login/donar-login.component';
import { DonarRegisterComponent } from './donar-register/donar-register.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';

const routes: Routes = [
  {path:'BloodBankManagementPortal', component: RegisterComponent},
  {path:'BloodBankManagementPortal/Admin', component: AdminComponent},
  {path:'BloodBankManagementPortal/Admin/AdminLogin', component: AdminLoginComponent},
  {path:'BloodBankManagementPortal/Donor', component: DonorComponent},
  {path:'BloodBankManagementPortal/Donor/DonarLogin', component: DonarLoginComponent},
  {path:'BloodBankManagementPortal/Donor/DonarRegister', component: DonarRegisterComponent},
  {path:'BloodBankManagementPortal/Requestor', component: RequestorComponent},
  {path:'BloodBankManagementPortal/Requestor/UserLogin', component: UserLoginComponent},
  {path:'BloodBankManagementPortal/Requestor/UserRegister', component: UserRegisterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
