import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {
  private baseUrl = 'http://localhost:8080/api/BloodBankManagementPortal/Admin/AdminLogin';

  constructor(private http:HttpClient) { }

  adminLogin(admin:any):Observable<any>{
    return this.http.post(this.baseUrl,admin);
  }
}
