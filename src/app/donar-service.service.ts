import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
@Injectable({
providedIn: 'root'
})
export class DonarServiceService {
private baseUrl = 'http://localhost:8081/api/Requestor/DonarLogin';
constructor(private http: HttpClient) { }
createRequestor(requestor: any): Observable<any> {
return this.http.post(this.baseUrl, requestor);
 }
getRequestorsList(): Observable<any>{
return this.http.get(this.baseUrl);
 }
deleteRequestor(id: number): Observable<any> {
return this.http.delete(`${this.baseUrl}/${id}`);
 }
 
updateRequestor(requestor: Object): Observable<Object> {
return this.http.put(`${this.baseUrl}` + `/update`, requestor);
 }
getRequestor(id: number): Observable<any> {
return this.http.get(`${this.baseUrl}/${id}`);
 }
}